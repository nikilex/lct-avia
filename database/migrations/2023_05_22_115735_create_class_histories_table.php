<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_histories', function (Blueprint $table) {
            $table->date('SDAT_S')->nullable();
            $table->string('SAK')->nullable();
            $table->integer('FLT_NUM')->nullable();
            $table->date('DD')->nullable();
            $table->integer('SEG_NUM')->nullable();
            $table->string('SORG')->nullable();
            $table->string('SDST')->nullable();
            $table->string('SSCL1')->nullable();
            $table->string('SEG_CLASS_CODE')->nullable();
            $table->string('NBCL')->nullable();
            $table->integer('FCLCLD')->nullable();
            $table->integer('PASS_BK')->nullable();
            $table->integer('SA')->nullable();
            $table->integer('AU')->nullable();
            $table->integer('PASS_DEP')->nullable();
            $table->integer('NS')->nullable();
            $table->integer('DTD')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_histories');
    }
}
