<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinalyPredictionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finaly_predictions', function (Blueprint $table) {
            $table->string('SORG')->nullable();
            $table->string('SDST')->nullable();
            $table->string('SSCL1')->nullable();
            $table->string('SEG_CLASS_CODE')->nullable();
            $table->integer('DTD')->nullable();
            $table->integer('DD_month')->nullable();
            $table->integer('DD_dayofyear')->nullable();
            $table->integer('DD_day')->nullable();
            $table->integer('DD_week')->nullable();
            $table->integer('DD_year')->nullable();
            $table->integer('SDAT_S_month')->nullable();
            $table->integer('SDAT_S_dayofyear')->nullable();
            $table->integer('SDAT_S_day')->nullable();
            $table->integer('SDAT_S_week')->nullable();
            $table->integer('SDAT_S_year')->nullable();
            $table->integer('tmax_WEEK_SORG')->nullable();
            $table->integer('tmax_MONTH_SORG')->nullable();
            $table->integer('tmax_WEEK_SDST')->nullable();
            $table->integer('tmax_MONTH_SDST')->nullable();
            $table->string('FREQ')->nullable();
            $table->integer('holiday_DD')->nullable();
            $table->integer('holiday_SDAT_S')->nullable();
            $table->string('trend_total')->nullable();
            $table->string('trend_class')->nullable();
            $table->string('trend_route')->nullable();
            $table->string('TT_DEP_HRS')->nullable();
            $table->string('TT_ARR_HRS')->nullable();
            $table->integer('CAP_J')->nullable();
            $table->integer('CAP_Y')->nullable();
            $table->string('demand')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finaly_predictions');
    }
}
