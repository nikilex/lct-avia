<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeasonalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seasonalities', function (Blueprint $table) {
            $table->string('SORG')->nullable();
            $table->string('SDST')->nullable();
            $table->integer('day')->nullable();
            $table->integer('month')->nullable();
            $table->integer('trend_route')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seasonalities');
    }
}
