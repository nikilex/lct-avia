<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDynamicClassHistoryView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement($this->createView());
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement($this->dropView());
    }

        /**
     * Reverse the migrations.
     *
     * @return void
     */
    private function createView(): string
    {
        return <<<SQL
            CREATE VIEW view_dynamic_class_history AS
                SELECT  
                NOW() as PRED_DATE, 
                SDAT_S,
                SEG_CLASS_CODE,
                FLT_NUM,
                (PASS_DEP + NS + GREATEST((PASS_BK - PASS_DEP  - NS), 0)) AS PRED_DEMAND
                FROM class_histories group by SDAT_S, FLT_NUM
            SQL;
    }

        /**
     * Reverse the migrations.
     *
     * @return void
     */
    private function dropView(): string
    {
        return <<<SQL

            DROP VIEW IF EXISTS `view_dynamic_class_history`;
            SQL;
    }
}
