<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePredictionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('predictions', function (Blueprint $table) {
            $table->date('PRED_DATE')->nullable();
            $table->date('SDAT_S')->nullable();
            $table->string('SEG_CLASS_CODE')->nullable();
            $table->string('FLT_NUM')->nullable();
            $table->string('PRED_DEMAND')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('predictions');
    }
}
