<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldInSeasonalities2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seasonalities', function (Blueprint $table) {
            $table->integer('trend_route_7d_rolling')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seasonalities', function (Blueprint $table) {
            $table->removeColumn('trend_route_7d_rolling');
        });
    }
}
