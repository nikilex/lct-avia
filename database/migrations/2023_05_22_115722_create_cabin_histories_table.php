<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCabinHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cabin_histories', function (Blueprint $table) {
            $table->date('DAT_S')->nullable();
            $table->string('SAK')->nullable();
            $table->integer('FLT_NUM')->nullable();
            $table->date('DEP_DATE')->nullable();
            $table->tinyInteger('FGNUM')->nullable();
            $table->string('SEG_ORIG')->nullable();
            $table->string('SEG_DEST')->nullable();
            $table->integer('TT_DEP')->nullable();
            $table->integer('TT_ARR')->nullable();
            $table->string('SSC')->nullable();
            $table->string('CAP')->nullable();
            $table->string('UAL')->nullable();
            $table->string('EQUIP')->nullable();
            $table->integer('DTD')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cabin_histories');
    }
}
