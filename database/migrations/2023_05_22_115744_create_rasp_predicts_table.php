<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRaspPredictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rasp_predicts', function (Blueprint $table) {
            $table->string('AIRLINE_CODESH')->nullable();
            $table->integer('FLT_NUMSH')->nullable();
            $table->string('LEG_ORIG')->nullable();
            $table->string('LEG_DEST')->nullable();
            $table->date('EFFV_DATE')->nullable();
            $table->date('DISC_DATE')->nullable();
            $table->string('FREQ')->nullable();
            $table->integer('NUM_LEGS')->nullable();
            $table->date('CAPTURE_DATE1')->nullable();
            $table->integer('DEP_TIME1')->nullable();
            $table->integer('ARR_TIME1')->nullable();
            $table->string('EQUIP1')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rasp_predicts');
    }
}
