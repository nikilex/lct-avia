<?php

namespace App\Imports;

use App\Models\ClassHistory;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

HeadingRowFormatter::default('none');

class ClassHistoryImport implements ToModel, WithChunkReading, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return User|null
     */
    public function model(array $row)
    {
        ini_set('max_execution_time', '300'); //300 seconds = 5 minutes
        return new ClassHistory([
           'SDAT_S'     => Carbon::createFromFormat('d.m.Y', $row['SDAT_S'])->format('Y-m-d'),
           'SAK'    => $row['SAK'],
           'FLT_NUM' => $row['FLT_NUM'],
           'DD' => Carbon::createFromFormat('d.m.Y', $row['DD'])->format('Y-m-d'),
           'SEG_NUM' => $row['SEG_NUM'],
           'SORG' => $row['SORG'],
           'SDST' => $row['SDST'],
           'SSCL1' => $row['SSCL1'],
           'SEG_CLASS_CODE' => $row['SEG_CLASS_CODE'],
           'NBCL' => $row['NBCL'],
           'FCLCLD' => $row['FCLCLD'],
           'PASS_BK' => $row['PASS_BK'],
           'SA' => $row['SA'],
           'AU' => $row['AU'],
           'PASS_DEP' => $row['PASS_DEP'],
           'NS' => $row['NS'],
           'DTD' => $row['NS']
        ]);
    }

        
    public function chunkSize(): int
    {
        return 1000;
    }
}