<?php

namespace App\Imports;

use App\Models\ClassHistory;
use App\Models\Prediction;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

HeadingRowFormatter::default('none');

class PredictionsImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return User|null
     */
    public function model(array $row)
    {
        return new Prediction([
           'PRED_DATE'      => Carbon::createFromFormat('Y-m-d', $row['PRED_DATE'])->format('Y-m-d'),
           'SDAT_S'         => Carbon::createFromFormat('Y-m-d', $row['SDAT_S'])->format('Y-m-d'),
           'SEG_CLASS_CODE' => $row['SEG_CLASS_CODE'],
           'FLT_NUM'        => $row['FLT_NUM'],
           'PRED_DEMAND'    => $row['PRED_DEMAND'],
        ]);
    }
}