<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PredictionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dateStart' => ['required'],
            'dateEnd'   => ['required']
        ];
    }

    public function messages()
    {
        return [
            'dateStart.required' => 'Дата начала обязательна для заполнения',
            'dateEnd.required'   => 'Дата окончания обязательна ля заполнения'
        ];
    }
}
