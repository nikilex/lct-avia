<?php

namespace App\Http\Controllers;

use App\Http\Requests\PredictionRequest;
use App\Http\Resources\PredictionListResource;
use App\Imports\PredictionsImport;
use App\Models\Prediction;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PredictionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $predictions = Prediction::paginate();
        return new ResourceCollection($predictions);
    }

    public function byFilters(PredictionRequest $request)
    {
        $validated = $request->validated();
        return Prediction::whereBetween('SDAT_S', [$validated['dateStart'], $validated['dateEnd']])->get();
    }

    public function import()
    {
        Excel::import(new PredictionsImport, 'datasets/predictions-3.csv');
        echo "Import complite";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new PredictionListResource(Prediction::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
