<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClassHistoryRequest;
use App\Http\Resources\ClassHistoryCollection;
use App\Http\Resources\ClassHistoryViewCollection;
use App\Imports\ClassHistoryImport;
use App\Models\ClassHistory;
use App\Models\ViewPredictionClassHistory;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\ExcelServiceProvider;

class ClassHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ClassHistoryCollection(ClassHistory::paginate());
    }

    public function byFilter(Request $request)
    {
        $endDate = Carbon::parse($request->startDate)->addMonths($request->period)->format('Y-m-d');
        $classHistories = ClassHistory::whereBetween('SDAT_S',[date($request->startDate), date($endDate)])
        ->when($request->seg_class_code, function($q) use ($request) {
            $q->where('SEG_CLASS_CODE', $request->seg_class_code);
        })
        ->when($request->flt_num, function($q) use ($request) {
            $q->where('FLT_NUM', $request->flt_num);
        })
        ->when($request->sorg, function($q) use ($request) {
            $q->where('SORG', $request->sorg);
        })
        
        ->when($request->sdst, function($q) use ($request) {
            $q->where('SDST', $request->sdst);
        })
        ->groupBy('SDAT_S', 'FLT_NUM')->orderBy('SDAT_S', 'ASC')->get();
        return new ClassHistoryViewCollection($classHistories);
    }

    public function import()
    {
        Excel::import(new ClassHistoryImport, 'datasets/class-history-for-front.csv');
        echo "Import complite";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
