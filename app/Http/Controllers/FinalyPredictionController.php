<?php

namespace App\Http\Controllers;

use App\Http\Resources\FinalyPredictionCollection;
use App\Http\Resources\PredictionListResource;
use App\Imports\PredictionsImport;
use App\Models\FinalyPrediction;
use App\Models\Prediction;
use App\Models\ViewFinalyPredictions;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Carbon;

class FinalyPredictionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $predictions = FinalyPrediction::paginate();
        return new FinalyPredictionCollection($predictions);
    }

    public function getFltNumByClassCode(Request $request)
    {
        return FinalyPrediction::select('FLT_NUMSH')
            ->where('SEG_CLASS_CODE', $request->seg_class_code)
            ->where('SORG', $request->sorg)
            ->where('SDST', $request->sdst)
            ->distinct()
            ->get()
            ->pluck('FLT_NUMSH');
    }

    public function byFilter(Request $request)
    {
        $endDate = Carbon::parse($request->startDate)->subMonths($request->period)->format('Y-m-d');
        $classHistories = FinalyPrediction::whereBetween('CAPTURE_DATE1', [date($endDate), date($request->startDate)])
            ->when($request->seg_class_code, function ($q) use ($request) {
                $q->where('SEG_CLASS_CODE', $request->seg_class_code);
            })
            ->when($request->flt_num, function ($q) use ($request) {
                $q->where('FLT_NUMSH', $request->flt_num);
            })
            ->when($request->sorg, function ($q) use ($request) {
                $q->where('SORG', $request->sorg);
            })
            ->when($request->sdst, function ($q) use ($request) {
                $q->where('SDST', $request->sdst);
            })
            ->get();
        return new FinalyPredictionCollection($classHistories);
    }

    public function toArray()
    {
        $predictions = FinalyPrediction::get()->toArray();
        return new FinalyPredictionCollection($predictions);
    }

    public function view()
    {
        return ViewFinalyPredictions::select("*")
            ->get()
            ->toArray();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
