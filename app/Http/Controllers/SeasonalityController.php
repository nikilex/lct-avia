<?php

namespace App\Http\Controllers;

use App\Http\Resources\PredictionListResource;
use App\Imports\PredictionsImport;
use App\Models\FinalyPrediction;
use App\Models\Prediction;
use App\Models\Seasonality;
use App\Models\ViewSeasonality;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Carbon;

class SeasonalityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $predictions = Seasonality::paginate();
        return new ResourceCollection($predictions);
    }

    public function toFilter(Request $request)
    {
        $endDate = Carbon::parse($request->startDate)->addMonths($request->period)->format('Y-m-d');
        return \DB::select('select * from finaly_predictions p 
        join seasonalities s 
        on s.SORG = p.SORG 
        and s.SDST = p.SDST 
        and s.day = day(p.CAPTURE_DATE1) 
        and s.month = month(p.CAPTURE_DATE1)
        WHERE p.CAPTURE_DATE1 >= "'.$request->startDate.'"
        and p.CAPTURE_DATE1 <= "'.$endDate.'"
        and p.SEG_CLASS_CODE = "'.$request->seg_class_code.'"
        and p.FLT_NUMSH = "'.$request->flt_num.'"
        and p.SORG = "'.$request->sorg.'"
        and p.SDST = "'.$request->sdst.'"
        ');
    }

    public function toArray()
    {
        $predictions = Seasonality::get();
        return new ResourceCollection($predictions);
    }

    public function view()
    {
        return ViewSeasonality::select("*")
                        ->get()
                        ->toArray();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
