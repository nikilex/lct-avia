<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class ClassHistoryViewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'PRED_DATE' => Carbon::now()->format('d.m.Y'),
            'SDAT_S' => $this->SDAT_S,
            'SEG_CLASS_CODE' => $this->SEG_CLASS_CODE,
            'FLT_NUM' => $this->FLT_NUM,
            'PRED_DEMAND' => ($this->PASS_DEP + $this->NS + max(($this->PASS_BK - $this->PASS_DEP - $this->NS), 0)),
        ];
    }
}
