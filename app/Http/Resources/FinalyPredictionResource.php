<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FinalyPredictionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'FLT_NUMSH' => $this->FLT_NUMSH,
            'SEG_CLASS_CODE' => $this->SEG_CLASS_CODE,
            'SORG' => $this->SORG,
            'SDST' => $this->SDST,
            'CAPTURE_DATE1' => $this->CAPTURE_DATE1,
            'PRED_DEMAND' => str_replace(',', '.', substr($this->demand,0,-2)),
        ];
    }
}
