<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewFinalyPredictions extends Model
{
    use HasFactory;

    public $table = "view_finaly_predictions";
}
