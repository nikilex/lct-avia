<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'SDAT_S',
        'SAK',
        'FLT_NUM',
        'DD',
        'SEG_NUM',
        'SORG',
        'SDST',
        'SSCL1',
        'SEG_CLASS_CODE',
        'NBCL',
        'FCLCLD',
        'PASS_BK',
        'SA',
        'AU',
        'PASS_DEP',
        'NS',
        'DTD'
    ];
}
