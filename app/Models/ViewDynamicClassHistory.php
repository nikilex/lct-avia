<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewDynamicClassHistory extends Model
{
    use HasFactory;

    public $table = "view_dynamic_class_history";
}
