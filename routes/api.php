<?php

use App\Http\Controllers\CabinHistoryController;
use App\Http\Controllers\ClassHistoryController;
use App\Http\Controllers\FinalyPredictionController;
use App\Http\Controllers\PredictionController;
use App\Http\Controllers\SeasonalityController;
use App\Http\Controllers\ViewDynamicClassHistoryController;
use App\Http\Resources\ClassHistoryCollection;
use App\Models\CabinHistory;
use App\Models\RaspPredict;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('predictions', PredictionController::class);
Route::get('predictions-filters', [PredictionController::class, 'byFilters']);
Route::apiResource('cabin-history', CabinHistoryController::class);

Route::apiResource('class-history', ClassHistoryController::class);
Route::get('class-history-filter', [ClassHistoryController::class, 'byFilter']);

Route::apiResource('rasp-prediction', RaspPredict::class);
Route::apiResource('dynamic-history', ViewDynamicClassHistoryController::class);
Route::get('finaly-predictions', [FinalyPredictionController::class, 'index']);
Route::get('finaly-predictions-filter', [FinalyPredictionController::class, 'byFilter']);
Route::get('finaly-predictions-array', [FinalyPredictionController::class, 'toArray']);
Route::get('finaly-predictions-view', [FinalyPredictionController::class, 'view']);
Route::get('seasonality', [SeasonalityController::class, 'index']);
Route::get('seasonality-filter', [SeasonalityController::class, 'toFilter']);
Route::get('seasonality-array', [SeasonalityController::class, 'toArray']);
Route::get('seasonality-view', [SeasonalityController::class, 'view']);

Route::get('get-flt-num-by-class-code', [FinalyPredictionController::class, 'getFltNumByClassCode']);
